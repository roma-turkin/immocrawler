import csv
import glob
import os
import time
import urllib
import sys
import configparser

class ImmoAnalyzer:
    LOGS_PATH = ''

    TOTAL_COST_MAX_SCORE = 0.0
    TOTAL_COST_BAD_MAX_SCORE = 0.0
    REFERENCE_TOTAL_COST = 0.0
    DEFAULT_RENT_COST = 0
    DEFAULT_EXTRA_COST = 0
    SQM_MAX_SCORE = 0
    SQM_BAD_MAX_SCORE = 0
    DEFAULT_SQM = 0
    REFERENCE_SQM = 0
    MAX_TARGET_SQM = 0
    TTW_MAX_SCORE = 0
    REFERENCE_TTW = 0

    def __init__(self):
        config_logs = configparser.ConfigParser()
        config_logs.read('../ImmoSpider/crawl.cfg')
        try:
            self.LOGS_PATH = config_logs['SELF_HOSTED_BOT']['LogPath']
        except:
            print("Failed to open ImmoSpider/crawl.cfg")
            sys.exit()

        config = configparser.ConfigParser()
        config.read('../analyzer/analyzer.cfg')
        try:
            self.TOTAL_COST_MAX_SCORE = float(config['SELF_HOSTED_BOT']['TOTAL_COST_MAX_SCORE'])
            self.TOTAL_COST_BAD_MAX_SCORE = float(config['SELF_HOSTED_BOT']['TOTAL_COST_BAD_MAX_SCORE'])
            self.REFERENCE_TOTAL_COST = float(config['SELF_HOSTED_BOT']['REFERENCE_TOTAL_COST'])
            self.DEFAULT_RENT_COST = float(config['SELF_HOSTED_BOT']['DEFAULT_RENT_COST'])
            self.DEFAULT_EXTRA_COST = float(config['SELF_HOSTED_BOT']['DEFAULT_EXTRA_COST'])
            self.SQM_MAX_SCORE = float(config['SELF_HOSTED_BOT']['SQM_MAX_SCORE'])
            self.SQM_BAD_MAX_SCORE = float(config['SELF_HOSTED_BOT']['SQM_BAD_MAX_SCORE'])
            self.DEFAULT_SQM = float(config['SELF_HOSTED_BOT']['DEFAULT_SQM'])
            self.REFERENCE_SQM = float(config['SELF_HOSTED_BOT']['REFERENCE_SQM'])
            self.TTW_MAX_SCORE = float(config['SELF_HOSTED_BOT']['TTW_MAX_SCORE'])
            self.REFERENCE_TTW = float(config['SELF_HOSTED_BOT']['REFERENCE_TTW'])
            self.MAX_TARGET_SQM = float(config['SELF_HOSTED_BOT']['MAX_TARGET_SQM'])
        except:
            print("Failed to open analyzer.cfg, or format is wrong")
            sys.exit()

    def get_apartments_dir(self):
        return os.path.abspath(self.LOGS_PATH)

    def clean_apartments_log(self):
        logs_path = self.get_apartments_dir()
        files = os.listdir(logs_path)
        for log in files:
            statinfo = os.stat(logs_path + '/' + log)
            if (statinfo.st_size == 0):
                os.remove(logs_path + '/' + log)

    def print_logs_list(self):
        logs_path = self.get_apartments_dir()
        files = os.listdir(logs_path)
        files.sort()
        for log in files:
            statinfo = os.stat(logs_path + '/' + log)
            print(log, " ", statinfo.st_size)

    def get_latest_apartments_file(self):
        logs_path = self.get_apartments_dir()
        files = os.listdir(logs_path)
        files.sort()
        last_file = open(logs_path + '/' + files[-1])
        return last_file

    def get_todays_apartments(self):
        logs_path = self.get_apartments_dir()
        files = os.listdir(logs_path)
        today_apartments = []
        now = time.time()
        today_start = now - now % 86400
        for f in files:
            file_time = os.path.getmtime(logs_path + '/' + f)
            if (file_time > today_start):
                aparts_csv = open(logs_path + '/' + f)
                aparts = self.extract_short_apartments_description(aparts_csv)
                today_apartments.append(aparts)
                aparts_csv.close()
        return today_apartments

    def get_n_hour_apartments(self, n):
        logs_path = self.get_apartments_dir()
        files = os.listdir(logs_path)
        n_hour_apartments = []
        now = time.time()
        for f in files:
            file_time = os.path.getmtime(logs_path + '/' + f)
            if (now - file_time < n * 3600):
                aparts_csv = open(logs_path + '/' + f)
                aparts = self.extract_short_apartments_description(aparts_csv)
                n_hour_apartments.append(aparts)
                aparts_csv.close()
        return n_hour_apartments
    
    def get_n_hour_apartments_over_score(self, n, min_score):
        logs_path = self.get_apartments_dir()
        files = os.listdir(logs_path)
        n_hour_apartments = []
        cnt = 0
        now = time.time()
        for f in files:
            file_time = os.path.getmtime(logs_path + '/' + f)
            if (now - file_time < n * 3600):
                aparts_csv = open(logs_path + '/' + f)
                entries = csv.DictReader(aparts_csv, delimiter=',')
                for entry in entries:
                    if (self.get_apartments_score(entry) > min_score):
                        n_hour_apartments.append(self.get_apartment_short_description(entry))
                        cnt += 1
                aparts_csv.close()
        return n_hour_apartments


    def get_apartment_short_description(self, entry):
        print(entry)
        apart=''.join("Addr: " + entry['address'])
        apart += ''.join(", SCORE=" + str(self.get_apartments_score(entry)))
        apart += ''.join(", Area=" + entry['sqm'])
        apart += ''.join(", Rent=" + entry['rent'])
        apart += ''.join(", ExtraCosts=" + entry['extra_costs'])
        apart += ''.join(", Zip=" + entry['zip_code'])
        apart += ''.join(", District=" + entry['district'])
#        apart += ''.join(", StartTime=" + entry['start_time'])	# Failed to find this field on the page
#        apart += ''.join(", TTW=" + entry['time_dest'])
        apart += ''.join(", URL=" + entry['url'] + "    ")
        return apart

    def extract_short_apartments_description(self, csv_file):
        entries = csv.DictReader(csv_file, delimiter=',')
        result = []
        for entry in entries:
            result.append(self.get_apartment_short_description(entry))
        return result

    def extract_json_descriptions(self, csv_file):
        entries = csv.DictReader(csv_file, delimiter=',')
        result = []
        for entry in entries:
            result.append(entry)
        return result

    def get_apartments_score(self, json_entry):
        '''
        Score is positive. The higher the value - the better the apartment.
        Score is calculated based on the following parameters:
        * rent + extra_costs
        * time_dest (time to work basically)
        * sqm
        Net cost of the apartment is a priority. Let it be max 5 points for the cheapest possible room (say, 500$)
        I want cheaper apartments to get higher score, so initial formula can be SCORE = 500 / (cost) * 5.
        total_cost > 200-check must be performed.

        Sqm has less high priority. Places I look for are typically 35-40 sqm. Let score be 2 * area / 40,where 40 is the average I expect.

        Time to work really matters, but somehow it sometimes gives errors. Let default value be 3 points for valid places with TTW less than 20 minutes. TTW > 100 is likely an error, and for all other places score is 20 min. / TTW * 3
        '''
        score = 0

        # 1. Calculate rental cost score
        rent = 0.0
        extra = 0.0
        try:
            rent = float(json_entry['rent'])
            extra = float(json_entry['extra_costs'])
            if (rent <= 0.0):
                rent = self.DEFAULT_RENT_COST
            if (extra <= 0.0):
                extra = self.DEFAULT_EXTRA_COST
        except:
            rent = self.DEFAULT_RENT_COST
            extra = self.DEFAULT_EXTRA_COST
        total_cost =  rent + extra
        if (total_cost >= self.REFERENCE_TOTAL_COST and total_cost <= 1.8 * self.REFERENCE_TOTAL_COST):
            score += self.REFERENCE_TOTAL_COST / total_cost * self.TOTAL_COST_MAX_SCORE
        else:
            score += self.REFERENCE_TOTAL_COST / total_cost * self.TOTAL_COST_BAD_MAX_SCORE

        #2. Calculate room area score.
        sqm = float(json_entry['sqm'])
        if (sqm <= 0.):
            sqm = self.DEFAULT_SQM
        if (sqm > self.MAX_TARGET_SQM):
            sqm = self.MAX_TARGET_SQM
        if (sqm >= 0.7*self.DEFAULT_SQM and sqm <= 1.4 * self.DEFAULT_SQM):
            score += self.SQM_MAX_SCORE * sqm / self.REFERENCE_SQM
        else:
            score += self.SQM_BAD_MAX_SCORE * sqm / self.REFERENCE_SQM

        #3. Calculate TTW score. #TODO: implement
 #       ttw = 100.0
 #       try:
 #           ttw = float(json_entry['time_dest'])
 #       except:
 #           ttw = 100.0
 #       if (ttw > 100.0):
 #           ttw = 100.0
        #score += self.TTW_MAX_SCORE / ttw * self.REFERENCE_TTW

        return score

if __name__ == '__main__':
    immo = ImmoAnalyzer()
    immo.get_apartments_dir()
#    immo.clean_apartments_log()
#    immo.get_todays_apartments()
#    csv_file = immo.get_latest_apartments_file()
#    entries = immo.extract_json_descriptions(csv_file)
#    print(immo.get_apartments_score(entries[0]))



