import requests
import configparser
import sys
from ImmoAnalyzer import ImmoAnalyzer 
from bottle import (
    Bottle, response, request as bottle_request
)

class BotHandlerMixin:
    BOT_URL = None

    def get_chat_id(self, data):
        chat_id = data['message']['chat']['id']
        return chat_id

    def get_command(self, data):
        return data['message']['text']

    def get_message(self, data):
        message_text = data['message']['text']
        return message_text

    def send_message(self, prepared_data):
        message_url = self.BOT_URL + 'sendMessage'
        requests.post(message_url, json=prepared_data)

class TelegramBot(BotHandlerMixin, Bottle):
    BOT_URL = ''
    CHAT_ID = ''
    MINIMAL_SCORE = 5
    UPDATE_PERIOD_MINUTES = 10.0
    def __init__(self, *args, **kwargs):
        super(TelegramBot, self).__init__()
        self.route('/', callback=self.post_handler, method='POST')
        config = configparser.ConfigParser()
        config.read('immocrawler.cfg')
        try:
            self.BOT_URL = config['SELF_HOSTED_BOT']['BotUrl'].strip('\'')
            self.CHAT_ID = config['SELF_HOSTED_BOT']['ChatId'].strip('\'')
            self.MINIMAL_SCORE = float(config['SELF_HOSTED_BOT']['MinimalInterestingScore'].strip('\''))
            self.UPDATE_PERIOD_MINUTES = float(config['SELF_HOSTED_BOT']['UpdatePeriod'].strip('\''))
        except:
            print("Failed to open configuration file")
            sys.exit()

    def send_today_apartments(self, chat_id):
        immo = ImmoAnalyzer()
        todays_apartments = immo.get_todays_apartments()
        aparts_descriptors = []
        cnt = 0
        for ap_array in todays_apartments:
            for ap in ap_array:
                json_aparts = {
                 "chat_id": chat_id,
                 "text": ap
                }  
                aparts_descriptors.append(json_aparts)
                self.send_message(json_aparts)
                cnt += 1
        return cnt

    def send_today_entries_count(self, chat_id):
        immo = ImmoAnalyzer()
        todays_apartments = immo.get_todays_apartments()
        cnt = 0
        for ap in todays_apartments:
            cnt+=1
        json_data = {
            "chat_id": chat_id,
            "text": cnt
        }
        self.send_message(json_data)
        return cnt

    def send_apartments_for_hour(self, chat_id):
        immo = ImmoAnalyzer()
        aparts_for_hour = immo.get_n_hour_apartments(1)
        aparts_descriptor = ''
        cnt = 0
        for ap_array in aparts_for_hour:
            for ap in ap_array:
                json_data = {
                    "chat_id": chat_id,
                    "text": ap
                }
                self.send_message(json_data)
                cnt += 1
        return cnt

    def send_good_apartments_for_last_update_period(self, chat_id):
        immo = ImmoAnalyzer()
        cnt = 0
        aparts_for_last_update_period = immo.get_n_hour_apartments_over_score((self.UPDATE_PERIOD_MINUTES - 0.5)/60.0, self.MINIMAL_SCORE)
        if (len(aparts_for_last_update_period) > 0):
            for ap in aparts_for_last_update_period:
                json_data = {
                    "chat_id": chat_id,
                    "text": ap
                }
                self.send_message(json_data)
                cnt += 1
        return cnt


    def post_handler(self):
        data = bottle_request.json
#        print(data)
        command = self.get_command(data)
        answer_data = ''
        immo = ImmoAnalyzer()
        immo.clean_apartments_log()
        cnt = 0
        if (command == '/today'):
            print('Processing today')
            cnt = self.send_today_apartments(self.CHAT_ID)
        elif (command == '/today_count'):
            cnt = self.send_today_entries_count(self.CHAT_ID)
        elif (command == '/hour'):
            print('Process requests for the last hour')
            cnt = self.send_apartments_for_hour(self.CHAT_ID)
        elif (command == '/urgent'):
            print('Process all interesting apartments for the last hour')
            cnt = self.send_good_apartments_for_last_update_period(self.CHAT_ID)
            return response
        else:
            cnt = -1

        if (cnt == 0):
            json_data = {
                "chat_id": self.CHAT_ID,
                "text": "no entries found"
            }
            self.send_message(json_data)
        if (cnt == -1):
            json_data = {
                "chat_id": self.CHAT_ID,
                "text": "wrong command"
            }
            self.send_message(json_data)

        return response

if __name__=='__main__':
    app = TelegramBot()
    app.run(host='localhost',port=8080,debug=True)

