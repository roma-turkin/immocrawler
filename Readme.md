# Intro
Core of the project has been inherited from ImmoSpider project: https://github.com/asmaier/ImmoSpider. At the moment of "forking" repository was at this commit: https://github.com/asmaier/ImmoSpider/tree/253c4e9193ceea7b6bd200af52770e35b2116202. Now it has significantly changed.
For installation of this bot first proceed with instructions from initial repository. In particular, setup python3, scrapy, and try to run the code of this guy.

Additional steps, found during deployment to the VPS:

* Run command "pip3 install --upgrade setuptools"

* In the requirements, replace "sklearn" by "scikit-learn"

* Run "sudo apt install libssl-dev"


This bot is made in a custom way. First part is the crawling part. It is handled via cron, which executes script placed at <repo_folder>/ImmoSpider/crawl.py. This script collects all entries, stores them to the local database, and also puts new entries to the <repo_folder>/data folder. This behavior is inherited from the initial repo.

Second part of the bot is bot itself. It processes incoming requests. Some are coming from Telegram client, some - from cron (executing the script periodically). From telegram one can send 4 commands at the moment. `/today` - get all commands from today. `/hour` - get all commands from the last hour. `/urgent` is buggy now, likely it will send all entries fitting your requirements that appeared in last 10 minutes (with default cron setup). `/today_count` will send count of all entries that bot has found today.

## Setup
This bot assumes you are familiar with telegram bots: you know how to get API token, how to set up a web-hook using ngrok and etc.

* install 'bootle' (pip3 install bottle)

* Clone this repo.

* Place ngrok executable to "bot" folder (mkdir ~/tmp && cd tmp && wget https://bin.equinox.io/c/4VmDzA7iaHb/ngrok-stable-linux-amd64.zip && unzip ngrok-stable-linux-amd64.zip && mv ngrok ~/immocrawler/bot - this one should work). 

* Create "data" folder at the root folder of the project

* Rename bot/immocrawler.cfg.tmpl to immocrawler.cfg and fill in valid token for your bot and your Chat ID. You get the token from the BotFather, and to find Chat ID, you can ask it from @get_id_bot.

* Rename ImmoSpider/crawl.cfg.tmpl to crawl.cfg and fill in valid values. SelfRequest should be a valid request that you send to your server, simulating message from your Telegram client (I simply copied a request from the phone for myself)

* Rename analyzer/analyzer.cfg.tmpl to analyzer.cfg and fill in the desired parameters. 

* Execute "crontab -e" command and set up cron properly. Example of crontab configuration is given in ImmoSpider/crontab_example.txt. 

* cd into "bot" folder

* configure ngrok (see instructions on the web-site)

* Start ngrok server (cd path/to/ngrok && ./ngrok http 8080 > /dev/null &)

* After ngrok start, user need the URL for the web-hook. I do it this way: export WEBHOOK_URL="$(curl http://localhost:4040/api/tunnels | jq ".tunnels[1].public_url")". After this command $WEBHOOK_URL contains the URL, which should be passed to the webhook setter for Telegram: https://api.telegram.org/bot[your-bot-token]/setWebhook?$WEBHOOK_URL. I will come up with an automatic script ASAP.

* Add path to ImmoAnalyzer to the python search path (I added a line export PYTHONPATH=$PYTHONPATH:/path/to/immo/immocrawler/analyzer to the .bash_profile) 

* Run bot with command "python3 bot.py". To run it in background, use nohup. 

### Usage
On the first run bot will crawl all immobilienscout24 entries, and all entries fitting your needs will be sent to your telegram account. Therefore make sure it is your chat id that will receive this DDoS attack, or you can be reported as a spammer.


