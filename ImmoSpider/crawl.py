import configparser
import time
import sys
import os

class ImmoscoutExecutor:
    IMMO_REQUEST=''
    LOG_PATH=''
    SELF_REQUEST = ''
    BOT_SERVER_URL = ''

    def __init__(self, *args, **kwargs):
        config = configparser.ConfigParser()
        config.read('crawl.cfg')
        try:
            self.IMMO_REQUEST = config['SELF_HOSTED_BOT']['Url'].strip('\'')
            self.LOG_PATH = config['SELF_HOSTED_BOT']['LogPath'].strip('\'')
            self.SELF_REQUEST = config['SELF_HOSTED_BOT']['SelfRequest'].strip('\'')
            self.BOT_SERVER_URL = config['SELF_HOSTED_BOT']['BotUrl'].strip('\'')
        except:
            print("Crawl.py: failed to open configuration file")
            sys.exit()

    def execute_search_script(self):
        command = 'scrapy crawl immoscout -o '
        ts = time.gmtime()
        timestamp = time.strftime("%Y_%m_%d_%H_%M_%S",ts)
        log_file_name = '\"' + self.LOG_PATH + 'apartment_' +  timestamp + '.csv' + '\"'
        command += log_file_name
        command += ' -a url='
        command += self.IMMO_REQUEST
        command += ' -L INFO'
        print('Executing shell command: ' + command)
        os.system(command) 

    def trigger_bot_announcement(self):
        request = 'curl --header \"Content-Type: application/json\" '
        request += '--request POST --data \''
        request += self.SELF_REQUEST
        request += '\' ' 
        request += self.BOT_SERVER_URL
        os.system(request)

if __name__=='__main__':
    executor = ImmoscoutExecutor()
    executor.execute_search_script()
    executor.trigger_bot_announcement()


